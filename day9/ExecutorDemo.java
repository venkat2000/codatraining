package day9;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {
	public static void main(String[] args) throws Exception {
		ExecutorService ex = Executors.newFixedThreadPool(3);
		ex.execute(() -> {
			System.out.println("Child thread");
		});
		ex.execute(new MyRunnable());
		Future f = ex.submit(new MyCallable());
		ex.shutdown();
	}
}

class MyRunnable implements Runnable {
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}

class MyCallable implements Callable {
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return "hello";
	}
}