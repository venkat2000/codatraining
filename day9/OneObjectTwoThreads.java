package day9;

public class OneObjectTwoThreads {
	public static void main(String[] args) {
		ReservationCounter counter = new ReservationCounter();
		new Thread(() -> {
			synchronized (counter) {
				counter.bookTicket(1000);
				counter.giveChange();				
			}
		}, "ramu").start();
		new Thread(() -> {
			synchronized (counter) {
				counter.bookTicket(500);
				counter.giveChange();				
			}
		}, "somu").start();
	}
}

class ReservationCounter {
	int amt;
	public void bookTicket(int amt) {
		this.amt = amt;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Ticket booked for " + name + " amount = " + amt);
	}
	public void giveChange() {
		int change = amt - 100;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Change given to " + name + " amount = " + change);
	}
}