package day9;

public class DeadlockDemo {
	public static void main(String[] args) {
		Frog frog = new Frog();
		Crane crane = new Crane();
		new Thread(() -> {
			crane.eat(frog);
		}).start();
		new Thread(() -> {
			frog.escape(crane);
		}).start();
	}
}

class Frog {
	synchronized public void escape(Crane c) {
		c.cranesLeaveMethod();
	}
	synchronized public void frogsLeaveMethod( ) {
		
	}
}

class Crane {
	synchronized public void eat(Frog f) {
		System.out.println("eating........");
		System.out.println("eating........");
		f.frogsLeaveMethod();
		System.out.println("eating........");
	}
	synchronized public void cranesLeaveMethod() {
		
	}
}