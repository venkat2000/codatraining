package day9;

public class OneObjectTwoThreadsOneTask {
	public static void main(String[] args) {
		Gun gun = new Gun();
		new Thread(() -> {
			for (int i = 0; i < 5; i++) {
				gun.fill();
			}
		}, "filler").start();
		new Thread(() -> {
			for (int i = 0; i < 5; i++) {
				gun.shoot();
			}
		}, "shooter").start();
	}
}

class Gun {
	boolean flag;
	synchronized public void fill() {
		if (flag) {
			try {
				wait();
			}
			catch (Exception e) {
				
			}
		}
		System.out.println("filling the gun");
		flag = true;
		notify();
	}
	synchronized public void shoot() {
		if (!flag) {
			try {
				wait();
			}
			catch (Exception e) {
				
			}
		}
		System.out.println("Shooting the gun");
		flag = false;
		notify();
	}
}