package day9;

public class OneThreadOneObject {
	public static void main(String[] args) {
		new Thread(() -> {
			Resource r = Factory.getResource();
			r.name = "first thread object";
			Resource r2 = Factory.getResource();
			System.out.println(r2.name);
		}).start();;
		new Thread(() -> {
			Resource resource = Factory.getResource();
			System.out.println(resource.name);
		}).start();;
	}
}

class Factory {
	private static ThreadLocal tLocal = new ThreadLocal();
	public static Resource getResource() {
		Resource r = (Resource)tLocal.get();
		if (r == null) {
			r = new Resource();
			tLocal.set(r);
			return r;
		}
		else return r;
	}
	public static void removeFromThread() {
		if (tLocal.get() != null) tLocal.remove();
	}
}

class Resource {
	String name;
	public Resource() {
		System.out.println("Resource constructor called");
	}
}


