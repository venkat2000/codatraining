package day7;

public class MethodRefDemo {
	public static void main(String[] args) {
		Func1 fadd = new AddBazar()::add;
		int sum = fadd.add(10,  20);
		System.out.println(sum);
		
		Func1 faddstat = AddBazar::addstatic;
		int sum2 = faddstat.add(20,  20);
		System.out.println(sum2);
	}
}

interface Func1 {
	public int add(int i, int j);
}

class AddBazar {
	public int add(int i, int j) {
		return i + j;
	}
	public static int addstatic	(int i, int j) {
		return i + j;
	}
}
