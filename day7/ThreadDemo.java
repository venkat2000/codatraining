package day7;

public class ThreadDemo {
	public static void main(String[] args) {
		new Thread(() -> {
			System.out.println("child thread");
		});
		System.out.println("main thread");
	}
}
