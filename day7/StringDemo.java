package day7;

public class StringDemo {
	public static void main(String[] args) {
		String s = "hello world";
		String str = new String(s);
		
		char c[] = {'h', 'i'};
		String str1 = new String(c);
		
		StringBuffer sbf = new StringBuffer("Hello world");
		StringBuilder sdl = new StringBuilder("Hello world");
		
		// StringBuffer - old - synchronized - slower
		// StringBUilder - new - random - fast
		String sf = String.format("The string is %s and the number is %d", "Hello world", 100);
		System.out.printf("Floating point number with 3 decimals %.3f", 10.0123455);
		System.out.printf("%-12s%-12s%s\n", "Column 1", "Column 2", "Column 3");
		System.out.printf("%-12.5s%s", "Hello world", "world");
		
	}
	public void work (int ...a) {
		for (int x : a)
			System.out.println(x);
	}
}
