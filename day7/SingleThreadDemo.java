package day7;

public class SingleThreadDemo {
	public static void main(String[] args) throws Exception {
		Thread t = Thread.currentThread();
		t.setName("venkat");
		t.setPriority(100);
		System.out.println(t);
		for (int i = 0; i < 5; i++) {
			System.out.println(i);
			Thread.sleep(1000);
		}
		Thread t2 = new Thread(() -> {
			System.out.println("Child thread");
		});
		Thread t3 = new Thread(new MyRunnable()); // traditional way
		new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println("child thread");
			}
		}).run();
	}
}

class MyRunnable implements Runnable {
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println("Child thread");
	}
}