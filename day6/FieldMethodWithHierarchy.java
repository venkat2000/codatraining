package day6;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodWithHierarchy {
	public static void main(String[] args) throws Exception {
		Test1 obj = new Test1();
		TestClass t = new TestClass();
		t.execute(obj);
	}
}

class TestClass {
	public void execute (Object p) throws Exception {
		Class c = p.getClass();
		Method[] m = c.getMethods();
		for (Method x : m)
			System.out.println(x);
		Method[] m1 = c.getDeclaredMethods(); // gets all methods not only public
	}
}
abstract class Parent {
	abstract public void display();
	public void check() {
		
	}
}

class Test1 extends Parent {
	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("Test1");
	}
}

class Test2 extends Parent {
	@Override
	private void display() {
		// TODO Auto-generated method stub
		System.out.println("Test2");
	}
}