package day6;

import java.lang.ref.WeakReference;

public class GCDemo {
	public static void main(String[] args) {
		Runtime r = Runtime.getRuntime();
		System.out.println("Before memory: " + r.freeMemory());
		GrandFather g = new GrandFather();
		System.out.println("After memoey " + r.freeMemory());
		WeakReference<T> <GrandFather> weak = new WeakReference<GrandFather>(g);
		g = null;
		System.out.println("After null " + r.freeMemory());
		g.gc();
		System.out.println("After gc " + r.freeMemory());
		g = weak.get();
		System.out.println(g.age);
	}
}

class GrandFather {
	String age;
	public GrandFather() {
		for (int i = 0; i < 10000; i++)
			age = new String();
	}
	
	private String getGold() {
		return "The gold is " + gold;
	}
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		System.out.println("finalize called " + getGold());
	}
}