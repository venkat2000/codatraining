package day6;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassDemo {
	public static void main(String[] args) throws Exception {
		PoliceStation p1 = new PoliceStation();
		Politician ramu = new Politician();
		Naxalite nax = new Naxalite();
		p1.arrest(nax);
	}
}

class PoliceStation {
	public void arrest (Object p) throws Exception {
		Class c = p.getClass();
		Field field = c.getField("name");
		System.out.println(field.get(p));
		Method met = c.getMethod("work", new Class[] {String.class});
		met.invoke(p, new Object[] {"hello world"});
		Method methods[] = c.getMethods();
	}
}

class Naxalite {
	public String name = "I am a naxalite";
	public void work(String s) {
		System.out.println("The work is " + s);
	}
}
class Politician {
	public String name = "I am a politician";
	public void work (String s) {
		System.out.println("I do social work");
	}
}