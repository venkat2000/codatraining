package day6;

public class PrototypeDemo {
	public static void main(String[] args) {
		Sheep mothersheep = new Sheep();
		Sheep dolly = mothersheep.createProto();
		mothersheep.name = "This is mother sheep";
		dolly.name = "This is dolly sheep";
		System.out.println(mothersheep.name + ":" + dolly.name);
		
	}
}
class Sheep implements Cloneable {
	String name;
	public Sheep() {
		System.out.println("Sheep constructor is called");
	}
	public Sheep createProto() {
		try {
			return (Sheep)super.clone();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}