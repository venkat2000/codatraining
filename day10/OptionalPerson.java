package day10;


import java.util.Optional;

public class OptionalPerson {
 public static void main(String[] args) {
	 Person person=new Person("John","JAMES@GMAIL.COM");
System.out.println(person .getEmail().map(String::toLowerCase).orElse("Email Not Provided"));
}
}
class Person
{
	private String name;
	private String email;
	public Person(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public String getEmail() {
//		return email;
//	}
	public Optional <String> getEmail(){
	  return	Optional.ofNullable(email);
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
}