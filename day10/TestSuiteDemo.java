package junitDemo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//Test Suite is a Convenient way to group together tests that are related
/*
@RunWith
� Used to invoke the class which is annotated to run the tests in that class

@Suite
� Allows you to manually build a suite containing tests from many classes
*/


@RunWith(Suite.class)
@SuiteClasses( {FirstJunitDemo.class ,  AnnotationsDemo.class})
public class TestSuiteDemo {


}
