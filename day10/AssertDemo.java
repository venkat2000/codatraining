package day10;

public class AssertDemo {
	public static void main(String[] args) {
		assert(1 == 10);
		// won't be enabled by default
		// -ea flag to enable assertions
	}
}
