package day5;

import java.util.Scanner;

public class InversionDemo {
	public static void main(String[] args) { 
		Child baby=new Child();
		Dog tiger=new Dog();
		Item item=new Stone();
		baby.playWithDog(tiger, item);
	}
}
class Child{
	public void playWithDog(Dog dog,Item item) {
		try{
			dog.play(item);
		}catch(DogExceptions de) {
			new Handler911().handle(de);
		}
	}
}
class Handler911{
	public void handle(DogExceptions de) {
		de.handle();
	}
}
class Dog{
	public void play(Item item)throws DogExceptions {
		item.execute();
	}
}
abstract class Item{
	public abstract void execute()throws DogExceptions;
}
class Stone extends Item{
	@Override
	public void execute()throws DogExceptions {
		throw new DogBiteException("you beat I bite....");		
	}
}
class Stick extends Item{
	@Override
	public void execute() throws DogExceptions{
		throw new DogBarkException("you throw I bark.............")		;
	}
}
class Biscuit extends Item{
	@Override
	public void execute() throws DogExceptions{
		throw new DogHappyException("yummy yummy I love biscuits....")		;
	}
}
//how to write custom exception
abstract class DogExceptions extends Exception{
	abstract public void handle();
}
class DogBiteException extends DogExceptions{
	@Override
	public void handle() {
		// TODO Auto-generated method stub
		System.out.println("Bite Exception Handled");
	}
	private String msg;
	public DogBiteException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
}
class DogBarkException extends DogExceptions{
	@Override
	public void handle() {
		// TODO Auto-generated method stub
		System.out.println("bark exception handled");
	}
	private String msg;
	public DogBarkException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
}
class DogHappyException extends DogExceptions{
	@Override
	public void handle() {
		// TODO Auto-generated method stub
		System.out.println("happy exception handled");
	}
	private String msg;
	public DogHappyException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
}