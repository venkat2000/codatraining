package day5;

public class CustomExceptionDemo {
	public static void main(String[] args) {
		
	}
}

class TestCustomException {
	public void test() throws Throwable {
		// skips compile time checks for exception handling
		// conveys the caller that this method is capable of throwing such exception
		// ensures that the calling method handles or throws the exception
		throw new MyException("this is a custom exception");
	}
}

class MyException extends Throwable{
	String msg;
	public MyException(String msg) {
		this.msg = msg;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return msg;
	}
}