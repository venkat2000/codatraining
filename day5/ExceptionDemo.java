package day5;

public class ExceptionDemo {
	public static void main(String[] args) {
		System.out.println("Before exception");
		try {
			int i = 2 / 0;
		}
		catch (ArithmeticException e) {
			System.out.println(e);
		}
		finally {
			System.out.println("Finally called");
		}
		System.out.println("OUtside exception");
	}
}
