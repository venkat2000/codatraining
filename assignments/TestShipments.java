package assignments;


import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class TestShipments {
	@Test
	public void normalTest() {
		LocalDate ld = LocalDate.parse("2020-03-26");
		LocalTime lt = LocalTime.parse("12:00");
		Cargo c1 = new Cargo("Person", ld, lt, 10, 5, 30);
		LocalDate estDate = LocalDate.parse("2020-03-27");
		LocalTime estTime = LocalTime.parse("10:00");
		DeliveryDateTime est = new DeliveryDateTime(estDate, estTime);
		DeliveryDateTime res = c1.EstimateDelivery();
		assertEquals(est.d, res.d);
		assertEquals(est.t, res.t);
	}
	
	@Test
	public void leapyearTest() {
		LocalDate ld = LocalDate.parse("2020-02-29");
		LocalTime lt = LocalTime.parse("12:00");
		Cargo c1 = new Cargo("Person", ld, lt, 1, 5, 30);
		LocalDate estDate = LocalDate.parse("2020-03-02");
		LocalTime estTime = LocalTime.parse("07:00");
		DeliveryDateTime est = new DeliveryDateTime(estDate, estTime);
		DeliveryDateTime res = c1.EstimateDelivery();
		assertEquals(est.d, res.d);
		assertEquals(est.t, res.t);
	}
	
	@Test
	public void yearendTest() {
		LocalDate ld = LocalDate.parse("2019-12-31");
		LocalTime lt = LocalTime.parse("12:00");
		Cargo c1 = new Cargo("Person", ld, lt, 10, 5, 30);
		LocalDate estDate = LocalDate.parse("2020-01-02");
		LocalTime estTime = LocalTime.parse("10:00");
		DeliveryDateTime est = new DeliveryDateTime(estDate, estTime);
		DeliveryDateTime res = c1.EstimateDelivery();
		assertEquals(est.d, res.d);
		assertEquals(est.t, res.t);
	}
	
	@Test
	public void timezoneTest() {
		LocalDate ld = LocalDate.parse("2019-12-31");
		LocalTime lt = LocalTime.parse("12:00");
		Cargo c1 = new Cargo("Person3", ld, lt, 10, 4, 30);
		LocalDate estDate = LocalDate.parse("2020-01-02");
		LocalTime estTime = LocalTime.parse("09:00");
		DeliveryDateTime est = new DeliveryDateTime(estDate, estTime);
		DeliveryDateTime res = c1.EstimateDelivery();
		assertEquals(est.d, res.d);
		assertEquals(est.t, res.t);
	}
}
