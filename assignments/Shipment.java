package assignments;


import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public class Shipment {
	public static void main(String[] args) {
//		Cargo c1=new Cargo("Grady Booch",LocalDate.now(),LocalTime.now(),24);
//		c1.EstimateDelivery();
//		Cargo c2=new Cargo("Eric Gamma",LocalDate.now(),LocalTime.now(),11);
//		c2.EstimateDelivery();
	}
}
class DeliveryDateTime {
	LocalDate d;
	LocalTime t;
	public DeliveryDateTime(LocalDate d, LocalTime t) {
		this.d = d;
		this.t = t;
	}
	
}
class Cargo{
	enum publicholidays{
		NEWYEAR(LocalDate.of(LocalDate.now().getYear(), 01, 01)),
		REPUBLICDAY(LocalDate.of(LocalDate.now().getYear(), 01, 15));
		
		private LocalDate ld;
		
		publicholidays(LocalDate ld) {
			this.ld=ld;
		}
		LocalDate getlocaldate() {
			return this.ld;
		}
	}
	String CustomerName;
	LocalDate Shipmentdate;
	LocalTime time;
	float NoOfHours;
	LocalDate Deliverydate;
	LocalTime end= LocalTime.of(18,0,0);
	int hoursChange, minChange;
	
	public Cargo(String name,LocalDate d,LocalTime t,int hrs, int hourChange, int minChange) {
		this.CustomerName=name;
		this.Shipmentdate=d;
		this.NoOfHours=hrs;
		System.out.println(t);
		t = t.plus(hourChange - 5, ChronoUnit.HOURS);
		t = t.plus(minChange - 30, ChronoUnit.MINUTES);
		System.out.println(t);
		this.time = t;
	}
	
	public DeliveryDateTime EstimateDelivery() {
		
		LocalDate d=Shipmentdate;
		LocalTime t=time;
		float h=NoOfHours;
		while(h>0) {
			d=CheckWorkingdayrnot(d);
			
			Duration duration = Duration.between(t, end);
			t=LocalTime.of(6,0,0);
			if(h<=duration.toHours())
				t=t.plus((long) h,ChronoUnit.HOURS);
			h-=duration.toHours();
			
			if(h>0)
				d=d.plus(1,ChronoUnit.DAYS);
			
		}
		System.out.println("Customer Name   :"+CustomerName+"\n"+"NO.of hours for delivery  :"+NoOfHours+"\n"+"Ordered on  :"+Shipmentdate);
		System.out.println("Estimated delivery date and Time: "+d+"\t"+t);
		System.out.println();
		return (new DeliveryDateTime(d, t));
			
	}
	
	public LocalDate CheckWorkingdayrnot(LocalDate d) {
		if(d.getDayOfWeek().getValue()==6 )
			d=d.plus(1,ChronoUnit.DAYS);
		if(d.getDayOfWeek().getValue()==7)
			d=d.plus(1,ChronoUnit.DAYS);
		
		for(publicholidays ph:publicholidays.values()) {
			if(d.equals(ph.getlocaldate()))
				d=d.plus(1,ChronoUnit.DAYS);
		}
		return d;
	}
}





